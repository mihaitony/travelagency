﻿using AgencyProject.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AgencyProject.Database
{
    public class ApplicationDbContext:DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
        : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<RoomFacility>().HasKey(x => new { x.RoomId, x.FacilityId });

            modelBuilder.Entity<RoomFacility>()
                .HasOne<Room>(x => x.Room)
                .WithMany(x => x.RoomFacilities)
                .HasForeignKey(x => x.RoomId);

            //modelBuilder.Entity<RoomFacility>()
            //    .HasOne<Facility>(x => x.Facility)
            //    .WithMany(x => x.RoomFacilities)
            //    .HasForeignKey(x => x.FacilityId);

            modelBuilder.Entity<HotelFacility>().HasKey(x => new { x.HotelId, x.FacilityId });

            modelBuilder.Entity<HotelFacility>()
                .HasOne<Hotel>(x => x.Hotel)
                .WithMany(x => x.HotelFacilities)
                .HasForeignKey(x => x.HotelId);

            //modelBuilder.Entity<HotelFacility>()
            //    .HasOne<Facility>(x => x.Facility)
            //    .WithMany(x => x.HotelFacilities)
            //    .HasForeignKey(x => x.FacilityId);
        }

        public virtual DbSet<Address> Address { get; set; }

        public virtual DbSet<City> City { get; set; }

        public virtual DbSet<Country> Country { get; set; }

        public virtual DbSet<Facility> Facility { get; set; }

        public virtual DbSet<Room> Rooms { get; set; }

        public virtual DbSet<RoomFacility> RoomFacilities { get; set; }

        public virtual DbSet<Hotel> Hotel { get; set; }

        public virtual DbSet<HotelFacility> HotelFacilities { get; set; }

        public virtual DbSet<Period> Period { get; set; }

        public virtual DbSet<FlyCompany> FlyCompany { get; set; }

        public virtual DbSet<Plane> Plane { get; set; }

        public virtual DbSet<Activity> Activity { get; set; }

        public virtual DbSet<Route> Route { get; set; }

    }
}
