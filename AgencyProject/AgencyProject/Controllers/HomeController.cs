﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using AgencyProject.Services.JsonReader;

namespace AgencyProject.Controllers
{
    public class HomeController : Controller
    {
        private readonly IJsonReader _reader;


        public HomeController(IJsonReader reader)
        {
            _reader = reader;
        }

        public IActionResult Index()
        { 
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult IndexTest()
        {
            var item = _reader.GetJsonFile().ToList();

            return View(item);
        }
    }
}
