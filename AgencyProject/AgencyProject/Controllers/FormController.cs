﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AgencyProject.Services.Repository;
using AgencyProject.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace AgencyProject.Controllers
{
    public class FormController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public FormController(IUnitOfWork unitOfwork)
        {
            _unitOfWork = unitOfwork;
        }

        public IActionResult Index()
        {
            return View();
        }
    }
}