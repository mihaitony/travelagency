﻿using AgencyProject.Models;
using AgencyProject.Services.Repository;
using AgencyProject.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace AgencyProject.Controllers
{
    public class HotelController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public HotelController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        
        [HttpGet]
        public IActionResult Index()
        {
            var items = _unitOfWork.HotelRepository.GetAll();
            
            return View(items);
        }

        [HttpGet]
        public IActionResult Details(int id)
        {
            var item =_unitOfWork.HotelRepository.Get(id);

            return View(item);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(HotelViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(viewModel);
            }

            Hotel model = new Hotel
            {
                Name = viewModel.Name,
                Description = viewModel.Description
            };

            _unitOfWork.HotelRepository.Add(model);
            _unitOfWork.Save();

            return RedirectToAction(nameof(Index));
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            var item = _unitOfWork.HotelRepository.Get(id);
            if(item == null)
            {
                return NotFound();
            }
            HotelViewModel viewModel = new HotelViewModel
            {
                Id = item.Id,
                Description = item.Description,
                Name = item.Name
            };
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(HotelViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(viewModel);
            }

            Hotel model = new Hotel
            {
                Id = viewModel.Id,
                Name = viewModel.Name,
                Description = viewModel.Description
            };

            _unitOfWork.HotelRepository.Update(model);
            _unitOfWork.Save();

            return RedirectToAction(nameof(Index));
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            var item = _unitOfWork.HotelRepository.Get(id);
            if (item == null)
            {
                return NotFound();
            }
            HotelViewModel viewModel = new HotelViewModel
            {
                Id = item.Id,
                Name = item.Name,
                Description = item.Description
            };
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(HotelViewModel viewModel)
        {
            var model = _unitOfWork.HotelRepository.Get(viewModel.Id);
            _unitOfWork.HotelRepository.Remove(model);
            _unitOfWork.Save();
            return RedirectToAction(nameof(Index));
        }
    }
}