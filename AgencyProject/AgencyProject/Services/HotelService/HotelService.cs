﻿using AgencyProject.Database;
using AgencyProject.Models;
using AgencyProject.Services.Repository;

namespace AgencyProject.Services.HotelService
{
    public class HotelService : Repository<Hotel>, IHotelService
    {
        public HotelService(ApplicationDbContext context) : base(context)
        {
        }
    }

}
