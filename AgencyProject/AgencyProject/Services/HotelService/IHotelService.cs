﻿using AgencyProject.Models;
using AgencyProject.Services.Repository;

namespace AgencyProject.Services.HotelService
{
    public interface IHotelService : IRepository<Hotel>
    {
        
    }
}
