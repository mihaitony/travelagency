﻿
namespace AgencyProject.Services.DbInitializer
{
    public interface IDbInitializer
    {
        void Seed();
    }
}
