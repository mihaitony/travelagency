﻿using AgencyProject.Models;
using AgencyProject.Services.JsonReader;
using AgencyProject.Services.Repository;
using System.Collections.Generic;

namespace AgencyProject.Services.DbInitializer
{
    public class DbInitilizer : IDbInitializer
    {
        private readonly IJsonReader _reader;
        private readonly IUnitOfWork _unitOfWork;

        public DbInitilizer(IJsonReader reader, IUnitOfWork unitOfWork)
        {
            _reader = reader;
            _unitOfWork = unitOfWork;
        }

        public void Seed()
        {
            if (!_unitOfWork.HotelRepository.IsEmpty())
            {
                IEnumerable<Hotel> Items = _reader.GetJsonFile();
                _unitOfWork.HotelRepository.AddRange(Items);
                _unitOfWork.Save();
            }
        }
    }
}
