﻿using AgencyProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AgencyProject.Services.JsonReader
{
    public interface IJsonReader
    {
        IEnumerable<Hotel> GetJsonFile();
    }
}
