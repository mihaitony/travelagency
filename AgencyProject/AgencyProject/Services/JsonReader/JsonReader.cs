﻿using AgencyProject.Models;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace AgencyProject.Services.JsonReader
{
    public class JsonReader : IJsonReader
    {
        private readonly PathOption _path;

        public JsonReader(IOptions<PathOption> path)
        {
            _path = path.Value;
        }

        public IEnumerable<Hotel> GetJsonFile()
        { 
            IEnumerable<Hotel> list = JsonConvert.DeserializeObject<List<Hotel>>(File.ReadAllText(_path.PathJson));

            return list;
        }
    }
}
