﻿using AgencyProject.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace AgencyProject.Services.Repository
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private readonly ApplicationDbContext _context;

        public Repository(ApplicationDbContext context)
        {
            _context = context;
        }

        public TEntity Get(int id)
        {
            var item = _context.Set<TEntity>().Find(id);

            return item;
        }

        public IEnumerable<TEntity> GetAll()
        {
            var list = _context.Set<TEntity>().ToList();

            return list;
        }

        public IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
        {
            var item = _context.Set<TEntity>().Where(predicate);

            return item;
        }

        public void Add(TEntity entity)
        {
            _context.Set<TEntity>().Add(entity);
        }

        public void Remove(TEntity entity)
        {
            _context.Set<TEntity>().Remove(entity);
        }

        public void AddRange(IEnumerable<TEntity> entities)
        {
            foreach(TEntity entity in entities)
            {
                _context.Add(entity);
            }
        }

        public void Update(TEntity entity)
        {
            _context.Set<TEntity>().Update(entity);
        }

        public void RemoveRange(IEnumerable<TEntity> entities)
        {
            _context.Set<TEntity>().RemoveRange(entities);
        }

        public bool IsEmpty()
        {
            var item = _context.Set<TEntity>().Any();

            return item;
        }
    }
}
