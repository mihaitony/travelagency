﻿using AgencyProject.Models;
using System;
using Hotel = AgencyProject.Models.Hotel;

namespace AgencyProject.Services.Repository
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<Hotel> HotelRepository { get; }
        IRepository<Formular> FormularRepository { get; }
        void Save();
    }
}
