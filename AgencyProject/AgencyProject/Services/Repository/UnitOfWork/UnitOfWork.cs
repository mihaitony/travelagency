﻿using AgencyProject.Database;
using AgencyProject.Models;
using System;


namespace AgencyProject.Services.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext _context;
        private bool disposed = false;

        public IRepository<Hotel> HotelRepository { get; }
        public IRepository<Formular> FormularRepository { get; }

        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;
            HotelRepository = new Repository<Hotel>(context);
            FormularRepository = new Repository<Formular>(context);
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
