﻿using AgencyProject.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AgencyProject.ViewModels
{
    public class FormularViewModel
    { 
        public int Id { get; set; }

        [Required(ErrorMessage = "Please select a country")]
        [Display(Name = "Country Name: ")]
        public List<Country> Country { get; set; }

        [Required(ErrorMessage = "Please select a city")]
        [Display(Name = "City Name: ")]
        public List<City> City { get; set; }

        [Required(ErrorMessage = "Please select the start date")]
        [Display(Name = "From: ")]
        public DateTime StartDate { get; set; }

        [Required(ErrorMessage = "Please select the finish date")]
        [Display(Name = "To: ")]
        public DateTime EndDate { get; set; }

        [Display(Name ="Budget: ")]
        public double Buget { get; set; }

        [Required(ErrorMessage = "Please select the fly company")]
        [Display(Name = "FlyCompany: ")]
        public List<FlyCompany> FlyCompany  { get; set; }

        [Required(ErrorMessage = "Please select the Plane")]
        [Display(Name = "Plane: ")]
        public List<Plane> Plane { get; set; }

        [Display(Name ="Number of stars: ")]
        public int NoOfStars { get; set; }

        public bool Jacuzzi { get; set; }

        public bool PetFriendly { get; set; }

        public bool Smooking { get; set; }
    }
}
