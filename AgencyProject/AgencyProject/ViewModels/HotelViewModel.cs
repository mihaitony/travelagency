﻿using System.ComponentModel.DataAnnotations;

namespace AgencyProject.ViewModels
{
    public class HotelViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Please enter the Hotel Name")]
        [MaxLength(20)]
        [Display(Name = "Hotel Name")]
        public string Name { get; set; }

        [Display(Name = "Hotel Description")]
        public string Description { get; set; }
    }
}
