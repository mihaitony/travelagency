﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AgencyProject.Models
{
    public class RoomFacility
    {
        public int RoomId { get; set; }
        public Room Room { get; set; }

        public int FacilityId { get; set; }
        public Facility Facility { get; set; }
    }
}
