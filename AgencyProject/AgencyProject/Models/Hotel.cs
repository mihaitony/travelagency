﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AgencyProject.Models
{
    public class Hotel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int NoOfStars { get; set; }

        public Address Address { get; set; }

        public ICollection<HotelFacility> HotelFacilities { get; set; }

        public ICollection<Room> Rooms { get; set; }
    }
}
