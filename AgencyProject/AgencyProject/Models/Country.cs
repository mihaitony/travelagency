﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AgencyProject.Models
{
    public class Country
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public Address Address { get; set; }

        //[ForeignKey("Address")]
        //public int AddressId { get; set; }
        //public Address Address { get; set; }

        public ICollection<City> Cities { get; set; }
    }
}
