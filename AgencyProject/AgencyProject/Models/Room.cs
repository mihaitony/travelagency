﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AgencyProject.Models
{
    public class Room
    {
        public int Id { get; set; }
        public int NoOfBeds { get; set; }
        public int Price { get; set; }

        public ICollection<RoomFacility> RoomFacilities { get; set; }

        [ForeignKey("Hotel")]
        public int HotelId { get; set; }
        public virtual Hotel Hotel { get; set; }

        public virtual Period Period { get; set; }
    }
}
