﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AgencyProject.Models
{
    public class Formular
    {
        public int Id { get; set; }

        public List<Country> Country { get; set; }

        public List<City> City { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public double Buget { get; set; }

        public List<Hotel> Hotel { get; set; }

        public List<FlyCompany> FlyCompany { get; set; }

        public List<Plane> Plane { get; set; }

        public bool Jacuzzi { get; set; }

        public bool PetFriendly { get; set; }

        public bool Smooking { get; set; }
    }
}
