﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AgencyProject.Models
{
    public class FlyCompany
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<Plane> Planes { get; set; }
    }
}
