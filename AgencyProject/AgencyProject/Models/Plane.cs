﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AgencyProject.Models
{
    public class Plane
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int NoOfSeats { get; set; }
        public double Price { get; set; }

        [ForeignKey("FlyCompany")]
        public int FlyCompanyId { get; set; }
        public FlyCompany FlyCompany { get; set; }

        public Activity Activity { get; set; }
    }
}
