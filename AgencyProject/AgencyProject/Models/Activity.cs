﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AgencyProject.Models
{
    public class Activity
    {
        public int Id { get; set; }
        public DateTime Departure { get; set; }

        [ForeignKey("Plane")]
        public int PlaneId { get; set; }
        public Plane Plane { get; set; }

        public ICollection<Route> Routes { get; set; }
    }
}
