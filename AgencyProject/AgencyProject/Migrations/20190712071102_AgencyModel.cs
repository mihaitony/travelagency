﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AgencyProject.Migrations
{
    public partial class AgencyModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "User");

            migrationBuilder.DropIndex(
                name: "IX_Period_RoomId",
                table: "Period");

            migrationBuilder.DropIndex(
                name: "IX_Address_HotelId",
                table: "Address");

            migrationBuilder.CreateIndex(
                name: "IX_Period_RoomId",
                table: "Period",
                column: "RoomId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Address_HotelId",
                table: "Address",
                column: "HotelId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Period_RoomId",
                table: "Period");

            migrationBuilder.DropIndex(
                name: "IX_Address_HotelId",
                table: "Address");

            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    Surname = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Period_RoomId",
                table: "Period",
                column: "RoomId");

            migrationBuilder.CreateIndex(
                name: "IX_Address_HotelId",
                table: "Address",
                column: "HotelId");
        }
    }
}
