﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AgencyProject.Migrations
{
    public partial class FinalModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Country_Address_AddressId",
                table: "Country");

            migrationBuilder.DropForeignKey(
                name: "FK_Plane_Activity_ActivityId",
                table: "Plane");

            migrationBuilder.DropIndex(
                name: "IX_Plane_ActivityId",
                table: "Plane");

            migrationBuilder.DropIndex(
                name: "IX_Country_AddressId",
                table: "Country");

            migrationBuilder.DropColumn(
                name: "ActivityId",
                table: "Plane");

            migrationBuilder.DropColumn(
                name: "AddressId",
                table: "Country");

            migrationBuilder.AddColumn<int>(
                name: "NoOfStars",
                table: "Hotel",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "CountryId",
                table: "Address",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "PlaneId",
                table: "Activity",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Address_CountryId",
                table: "Address",
                column: "CountryId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Activity_PlaneId",
                table: "Activity",
                column: "PlaneId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Activity_Plane_PlaneId",
                table: "Activity",
                column: "PlaneId",
                principalTable: "Plane",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Address_Country_CountryId",
                table: "Address",
                column: "CountryId",
                principalTable: "Country",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Activity_Plane_PlaneId",
                table: "Activity");

            migrationBuilder.DropForeignKey(
                name: "FK_Address_Country_CountryId",
                table: "Address");

            migrationBuilder.DropIndex(
                name: "IX_Address_CountryId",
                table: "Address");

            migrationBuilder.DropIndex(
                name: "IX_Activity_PlaneId",
                table: "Activity");

            migrationBuilder.DropColumn(
                name: "NoOfStars",
                table: "Hotel");

            migrationBuilder.DropColumn(
                name: "CountryId",
                table: "Address");

            migrationBuilder.DropColumn(
                name: "PlaneId",
                table: "Activity");

            migrationBuilder.AddColumn<int>(
                name: "ActivityId",
                table: "Plane",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "AddressId",
                table: "Country",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Plane_ActivityId",
                table: "Plane",
                column: "ActivityId");

            migrationBuilder.CreateIndex(
                name: "IX_Country_AddressId",
                table: "Country",
                column: "AddressId");

            migrationBuilder.AddForeignKey(
                name: "FK_Country_Address_AddressId",
                table: "Country",
                column: "AddressId",
                principalTable: "Address",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Plane_Activity_ActivityId",
                table: "Plane",
                column: "ActivityId",
                principalTable: "Activity",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
