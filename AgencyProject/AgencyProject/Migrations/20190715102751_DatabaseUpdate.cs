﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AgencyProject.Migrations
{
    public partial class DatabaseUpdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_City_Route_RouteId",
                table: "City");

            migrationBuilder.DropIndex(
                name: "IX_City_RouteId",
                table: "City");

            migrationBuilder.DropColumn(
                name: "RouteId",
                table: "City");

            migrationBuilder.DropColumn(
                name: "Arrival",
                table: "Activity");

            migrationBuilder.AddColumn<int>(
                name: "CityId",
                table: "Route",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Route_CityId",
                table: "Route",
                column: "CityId");

            migrationBuilder.AddForeignKey(
                name: "FK_Route_City_CityId",
                table: "Route",
                column: "CityId",
                principalTable: "City",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Route_City_CityId",
                table: "Route");

            migrationBuilder.DropIndex(
                name: "IX_Route_CityId",
                table: "Route");

            migrationBuilder.DropColumn(
                name: "CityId",
                table: "Route");

            migrationBuilder.AddColumn<int>(
                name: "RouteId",
                table: "City",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "Arrival",
                table: "Activity",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.CreateIndex(
                name: "IX_City_RouteId",
                table: "City",
                column: "RouteId");

            migrationBuilder.AddForeignKey(
                name: "FK_City_Route_RouteId",
                table: "City",
                column: "RouteId",
                principalTable: "Route",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
